package com.brunoshiroma.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {

  /**
   * Main method.
   * @param args for the program
   */
  public static void main(String[] args) {
    final SpringApplication app = new SpringApplication(Main.class);

    app.run(args);
  }

}
